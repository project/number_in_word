<?php

namespace Drupal\number_in_word;

/**
 * NumberInWordInterface.
 */
interface NumberInWordInterface {

  /**
   * Get a number in world.
   * The param is number int and float.
   */
  public function number_in_word(float $value);
}
