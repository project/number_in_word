<?php

namespace Drupal\number_in_word\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'number_in_word' formatter.
 *
 * @FieldFormatter(
 *   id = "number_in_word",
 *   label = @Translation("Number In Word"),
 *   field_types = {
 *     "integer",
 *     "decimal",
 *     "float",
 *   }
 * )
 */
class NumberInWord extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'unit' => 'Rupees',
      'unit_point' => 'Paise',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['unit'] = [
      '#title' => $this->t('Unit'),
      '#type' => 'textfield',
      '#description' => $this->t('Unit is Rupees, Dollar'),
      '#default_value' => $this->getSetting('unit'),
    ];
    $form['unit_point'] = [
      '#title' => $this->t('Unit Point'),
      '#type' => 'textfield',
      '#description' => $this->t('Unit is Paise, Cent'),
      '#default_value' => $this->getSetting('unit_point'),
    ];

    return $form;
    /*return [
    ] + parent::settingsForm($form, $form_state);*/
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $unit = $this->getSetting('unit');
      $point = $this->getSetting('unit_point');
      $elements[$delta] = [
        '#theme' => 'number_in_word',
        '#content' => $this->viewValue($item, $unit, $point)
      ];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   */
  protected function viewValue(FieldItemInterface $item, $unit, $point) {
    $value = \Drupal::service('number_in_word.number_in_word')->number_in_word($item->value, $unit, $point);
    return $value;
  }

}
